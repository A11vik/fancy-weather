export const renderTempOpt = (state: any) => {
  const { isCelsius } = state;
  return `
    <li class="options__temp ${isCelsius ? '' : 'options__temp--active'}" data-temp="f">&deg;F</li>
    <li class="options__temp ${isCelsius ? 'options__temp--active' : ''}" data-temp="c">&deg;C</li>
  `;
};
