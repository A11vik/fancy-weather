import { data, getIndex } from '../../../services/dataLang';
import { renderTempOpt } from './tempOpt.component';

export const renderHeader = (state: any) => {
  const { lang } = state;
  const idx = getIndex(lang);
  const { search, searchCity } = data;
  return `
    <h1 class="visuallyhidden">Weather app</h1>
    <div class="options">
      <button class="options__bg" id="btn-bg"></button>
      <select class="options__langs" id="lang" name="lang">
        <option ${lang === 'en' ? 'selected' : null} value="en">EN</option>
        <option ${lang === 'ru' ? 'selected' : null} value="ru">RU</option>
        <option ${lang === 'be' ? 'selected' : null} value="be">BE</option>
      </select>
      <ul class="options__temps">
        ${renderTempOpt(state)}
      </ul>
    </div>
    <div class="search">
      <input class="search__input" type="text" placeholder="${searchCity[idx]}" />
      <button class="search__btn">${search[idx]}</button>
    </div>
  `;
};
