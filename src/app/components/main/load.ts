// tslint:disable-next-line
/// <reference path="ymaps.d.ts" />

export function getMap(lat: any, long: any) {
  ymaps.ready(async () => {
    const ymap = document.getElementById('map');
    ymap.innerHTML = '';
    const myMap = new ymaps.Map(ymap, {
      center: [lat, long],
      controls: [],
      zoom: 7,
    });
    const placemark = new ymaps.Placemark(
      myMap.getCenter(),
      {
        balloonContent: '',
      },
      );
    myMap.geoObjects.add(placemark);
  });
}
