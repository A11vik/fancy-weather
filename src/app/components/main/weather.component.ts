import { fahrenheitFormula } from '../../../core/update';
import { data, getIndex } from '../../../services/dataLang';

export const renderWeather = ({ city, country, temp, feelsLike, humidity,
  wind, date, time, condition, tempNextDays, nameNextDays, isCelsius, lang }: any): string => {
  const idx: number = getIndex(lang);
  return `
    <div class="weather__title">
      <p class="weather__town">${city}, ${country}</p>
      <div class="weather__date">
        <p class="weather__time">${date}, ${time}</p>
      </div>
      <div class="weather__temp">
        <p class="weather__current" id="temp-current">${isCelsius ? temp : fahrenheitFormula(temp)}&deg;</p>
        <ul class="weather__list">
          <li class="weather__item" id="condition">${data.condition[condition][idx]}</li>
          <li class="weather__item" id="feels-like">${data.feelsLike[idx]}: ${isCelsius ? feelsLike : fahrenheitFormula(feelsLike)}</li>
          <li class="weather__item" id="wind">${data.wind[idx]}: ${wind} ${data.windScale[idx]}</li>
          <li class="weather__item" id="humidity">${data.humidity[idx]}: ${humidity}%</li>
        </ul>
      </div>
      <ul class="weather__next-days">
      <li class="weather__next">
        <span>${nameNextDays[0]}</span><br>
        <span class="weather__next-temp">${isCelsius ? tempNextDays[0] : fahrenheitFormula(tempNextDays[0])}&deg;</span>
      </li>
      <li class="weather__next">
        <span>${nameNextDays[1]}</span><br>
        <span class="weather__next-temp">${isCelsius ? tempNextDays[1] : fahrenheitFormula(tempNextDays[1])}&deg;</span>
      </li>
      <li class="weather__next">
        <span>${nameNextDays[2]}</span><br>
        <span class="weather__next-temp">${isCelsius ? tempNextDays[2] : fahrenheitFormula(tempNextDays[2])}&deg;</span>
      </li>
      </ul>
    </div>
  `;
};
