import { data, getIndex } from '../../../services/dataLang';

export const renderMap = (state: any) => {
  const { lat, long, lang } = state;
  const idx: number = getIndex(lang);
  const latObj = coordToDeg(lat);
  const longObj = coordToDeg(long);
  return `
    <div class="map__box" id="map"></div>
    <ul class="map__coords">
      <li class="map__coord" id="latitude">${data.lat[idx]}: ${latObj.deg}°${latObj.min}'</li>
      <li class="map__coord" id="longitude">${data.long[idx]}: ${longObj.deg}°${longObj.min}'</li>
    </ul>
  `;
};

export const coordToDeg = (coord: any) => {
  let dataCoord = coord;
  if (typeof coord === 'number') dataCoord = '' + dataCoord;
  const arr = dataCoord.split('.');

  const minNum = +arr[0] > 0 ? +arr[0] - +coord : +arr[0] - -coord;
  return {
    deg: arr[0],
    min: Math.round(Math.abs(minNum) * 60),
  };
};
