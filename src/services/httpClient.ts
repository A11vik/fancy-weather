class HttpClient {
  private servers: any;
  private proxy: string;

  constructor() {
    this.servers = {
      geo: {
        url: 'https://get.geojs.io/v1/ip/geo.json',
      },
      opencage: {
        key: 'b2499b11377846c3b6b9a616a82cf51a',
        url: 'https://api.opencagedata.com/geocode/v1/json?q=',
      },
      unsplash: {
        key: '06a5845d516e34e2d9f21de3d68bad63ea0ea722d6b4b5f2351a2f9d566b6db6',
        url: 'https://api.unsplash.com/photos/random?',
      },
      weather: {
        key: 'e441aa3b-fe12-4b65-b7e3-b363e023de13',
        url: 'https://api.weather.yandex.ru/v1/forecast?',
      },
    };
    this.proxy = 'https://cors-anywhere.herokuapp.com/';
  }

  public async get(server: string, args?: any) {
    let currentUrl: string;
    let header: object;
    const key: string = this.servers[server].key;
    const { lat, long, lang, condition, season, daytime } = args;

    switch (server) {
      case 'unsplash':
        const timeOfDay = daytime === 'd' ? 'day' : 'night';
        currentUrl = `${this.servers[server].url}client_id=${key}&query=${season},${condition},${timeOfDay}`;
        break;
      case 'weather':
        currentUrl = `${this.proxy}${this.servers[server].url}lat=${lat}&lon=${long}&${['limit=4']}&${['hours=false']}&${['extra=false']}`;
        header = {
          headers: {
            ['X-Yandex-API-Key']: key,
          },
        };
        break;
      case 'geo':
        currentUrl = `${this.servers[server].url}`;
        break;
      case 'opencage':
        currentUrl = `${this.servers[server].url}${lat}%2C%20${long}&key=${key}&language=${lang}`;
        break;
    }

    try {
      const response = await fetch(currentUrl, header);
      const data = await response.json();
      return data;
    } catch (err) {
      window.console.error('Err: ', err);
    }
  }

  public post() {
    const post = 'post';
  }

  public async put(city: string, lang: string) {
    try {
      if (city === 'undefined') return;
      const response = await fetch(`${this.servers.opencage.url}${city}&key=${this.servers.opencage.key}&language=${lang}`);
      const data = await response.json();
      return data;
    } catch (err) {
      window.console.error('Put error: ', err);
    }
  }
}

export const apiRequest = new HttpClient();
