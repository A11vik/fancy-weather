export const data: any = {
  feelsLike: ['feels like', 'ощущается как', 'адчуваецца як'],
  wind: ['wind', 'ветер', 'вецер'],
  humidity: ['humidity', 'влажность', 'вільготнасць'],
  lat: ['latitude', 'широта', 'шырата'],
  long: ['longitude', 'долгота', 'даўгата'],
  search: ['search', 'поиск', 'пошук'],
  searchCity: ['Search city', 'Поиск города' , 'Пошук горада'],
  windScale: ['m/s', 'м/с', 'м/з'],
  nameDays: {
    'понедельник': 'Панядзелак',
    'вторник':'Аўторак',
    'среда': 'Серада',
    'четверг': 'Чацвер',
    'пятница': 'Пятніца',
    'суббота': 'Субота',
    'воскресенье': 'Нядзеля',
  },
  condition: {
    clear: ['clear', 'ясно', 'Выразны'],
    ['partly-cloudy']: ['partly-cloudy', 'малооблачно', 'малавоблачна'],
    cloudy: ['cloudy', 'облачно с прояснениями', 'воблачна з праясненнямі'],
    overcast: ['overcast', 'пасмурно', 'пахмурна'],
    ['partly-cloudy-and-snow']: ['partly-cloudy-and-snow', 'снег', 'снег'],
    ['partly-cloudy-and-light-rain']: ['partly-cloudy-and-light-rain', 'небольшой дождь', 'невялікі дождж'],
    ['overcast-and-rain']: ['overcast-and-rain', 'сильный дождь', 'моцны дождж'],
    ['overcast-and-light-snow']: ['overcast-and-light-snow', 'небольшой снег', 'невялікі снег'],
    ['cloudy-and-light-snow']: ['cloudy-and-light-snow', 'небольшой снег', 'невялікі снег'],
    ['partly-cloudy-and-light-snow']: ['partly-cloudy-and-light-snow', 'небольшой снег', 'невялікі снег'],
    ['overcast-and-snow']: ['overcast-and-snow', 'снегопад', 'снегапад'],
    ['overcast-thunderstorms-with-rain']: ['overcast-thunderstorms-with-rain', 'сильный дождь, гроза', 'моцны дождж, навальніца'],
    ['cloudy-and-light-rain']: ['cloudy-and-light-rain', 'небольшой дождь', 'невялікі дождж'],
    ['overcast-and-light-rain']: ['overcast-and-light-rain', 'небольшой дождь', 'невялікі дождж'],
    ['cloudy-and-rain']: ['cloudy-and-rain', 'дождь', 'дождж'],
    ['overcast-and-wet-snow']: ['overcast-and-wet-snow', 'дождь со снегом', 'дождж са снегам'],
    ['cloudy-and-snow']: ['cloudy-and-snow', 'снег', 'снег'],
  },
};

export const getIndex = (lang: string): number => {
  switch (lang) {
    case 'en':
      return 0;
      break;
    case 'ru':
      return 1;
      break;
    case 'be':
      return 2;
      break;
  }
};
