import '../scss/base';

import { getMap } from '../app/components/main/load';
import { updateDom, updateSearchBox, updateTempScale } from '../core/update';
import { apiRequest } from '../services/httpClient';
import { render } from './render';

const fetchData: any = {
  city: '',
  condition: '',
  country: '',
  date: '',
  daytime: '',
  feelsLike: 0,
  humidity: 0,
  isCelsius: true,
  lang: 'en',
  lat: 0,
  long: 0,
  nameNextDays: [],
  season: '',
  temp: 0,
  tempNextDays: [],
  time: 0,
  timeZone: '',
  wind: '',
};

const getNamesOfDays = (date: any, timeZone: any): void => {
  const weekDays: string[] = [];
  for (let i = 0; i < 7; i += 1) {
    weekDays.push(date.toLocaleDateString(fetchData.lang, { weekday: 'long' }));
    date.setDate(date.getDate() + 1);
  }
  let currentIndex: number = weekDays.indexOf(date.toLocaleString(fetchData.lang, {
    timeZone,
    weekday: 'long',
  })) + 1;
  for (let i = 0; i < 3; i += 1) {
    if (currentIndex > weekDays.length - 1) currentIndex = 0;
    fetchData.nameNextDays.push(weekDays[currentIndex]);
    currentIndex += 1;
  }
};

const getTime = (lang: string, timeZone?: string): void => {
  fetchData.nameNextDays = [];
  const date: Date = new Date();
  const options: object = { timeZone, weekday: 'short', day: 'numeric' , month: 'long' };
  const timeOptions: object = { timeZone, hour12: false, hour: 'numeric', minute: 'numeric' };
  fetchData.date = date.toLocaleString(lang, options);
  fetchData.time = date.toLocaleString(lang, timeOptions);
  getNamesOfDays(date, timeZone);
};

const getWeather = async () => {
  const weather = await apiRequest.get('weather', {
    lat: fetchData.lat,
    long: fetchData.long,
  });
  const { condition, feels_like, humidity, season, temp, wind_speed, daytime } = weather.fact;
  const { name } = weather.info.tzinfo;
  fetchData.condition = condition;
  fetchData.feelsLike = feels_like;
  fetchData.humidity = humidity;
  fetchData.season = season;
  fetchData.temp = temp;
  fetchData.wind = wind_speed;
  fetchData.daytime = daytime;
  fetchData.timeZone = name;
  getTime(fetchData.lang, fetchData.timeZone);

  fetchData.tempNextDays = [];
  const forecasts = weather.forecasts;
  forecasts.slice(1).forEach((day: any) => {
    fetchData.tempNextDays = [...fetchData.tempNextDays, day.parts.day_short.temp];
  });
};

const getRegion = async () => {
  const region = await apiRequest.get('opencage', fetchData);
  let currCity, currCountry;
  if (region.results[0].components._type === 'city') {
    const { city, country, town, village } = region.results[0].components;
    currCity = city || town || village;
    currCountry = country;
  } else {
    const { state, country, village, unknown, city, town } = region.results[0].components;
    currCity = city || town || village || unknown || state;
    currCountry = country;
  }
  fetchData.city = currCity;
  fetchData.country = currCountry;
};

const getBackground = async () => {
  const bg = await apiRequest.get('unsplash', fetchData);
  const img = new Image();

  img.src = bg.urls.regular;

  img.onload = () =>
    document.body.style.backgroundImage = `url('${img.src}')`;
};

const getCoords = async () => {
  const { latitude, longitude } = await apiRequest.get('geo', fetchData);
  fetchData.lat = latitude;
  fetchData.long = longitude;
};

const getData = async () => {
  // getTime(fetchData.lang);

  await getCoords();
  await getWeather();
  await getRegion();
};

const changeTempScale = (evt: Event): void => {
  let selected: HTMLElement = document.querySelector('.options__temp--active');
  const element = evt.target as HTMLElement;
  if (element.classList.contains('options__temp--active')) return;
  selected.classList.remove('options__temp--active');
  element.classList.add('options__temp--active');
  fetchData.isCelsius = element.dataset.temp === 'c';
  selected = element;
  localStorage.setItem('isCelsius', fetchData.isCelsius);

  updateTempScale(fetchData);
};

const searchCity = async () => {
  const cityFromInput = document.querySelector('.search__input') as HTMLFormElement;
  if (cityFromInput.value.length < 2) return;
  const data = await apiRequest.put(cityFromInput.value, fetchData.lang);
  if (data.results.length) {
    const { lat, lng } = data.results[0].geometry;
    let currCity, currCountry;
    if (data.results[0].components._type === 'city') {
      const { city, country, town, village } = data.results[0].components;
      currCity = city || town || village;
      currCountry = country;
    } else {
      const { state, country, village, unknown, city, town } = data.results[0].components;
      currCity = city || town || village || unknown || state;
      currCountry = country;
    }
    fetchData.lat = lat;
    fetchData.long = lng;
    fetchData.city = currCity;
    fetchData.country = currCountry;

    await getWeather();
    await updateDom(fetchData);
    getMap(fetchData.lat, fetchData.long);
    cityFromInput.value = '';
  } else {
    window.alert("sorry, but your city can't be found");
  }
};

async function changeLang() {
  fetchData.lang = this.value;
  localStorage.setItem('lang', fetchData.lang);
  getTime(fetchData.lang, fetchData.timeZone);
  const data = await apiRequest.put(fetchData.city, fetchData.lang);
  if (data.results.length) {
    let currCity, currCountry;
    if (data.results[0].components._type === 'city') {
      const { city, country, town, village } = data.results[0].components;
      currCity = city || town || village;
      currCountry = country;
    } else {
      const { state, country, village, unknown, city, town } = data.results[0].components;
      currCity = city || town || village || unknown || state;
      currCountry = country;
    }
    fetchData.city = currCity;
    fetchData.country = currCountry;
    await updateDom(fetchData);
    updateSearchBox(fetchData.lang);
  } else {
    window.alert("sorry, but can't find localization for you city");
  }
}

const updateTime = () => {
  const prevTime = fetchData.time;
  getTime(fetchData.lang, fetchData.timeZone);
  if (prevTime === fetchData.time) return;
  const timeDom = document.querySelector('.weather__time');
  timeDom.textContent = `${fetchData.date}, ${fetchData.time}`;
};

const submitForm = async (e: any) => {
  if (e.keyCode === 13) {
    await searchCity();
  }
  return false;
};

export const appInit = async () => {
  fetchData.lang = localStorage.getItem('lang') || fetchData.lang;
  if (localStorage.getItem('isCelsius') === null) {
    fetchData.isCelsius = fetchData.isCelsius;
  } else {
    fetchData.isCelsius = localStorage.getItem('isCelsius') === 'true';
  }
  await getData();
  await render(fetchData);
  await getBackground();
  getMap(fetchData.lat, fetchData.long);

  document.querySelector('.options__temps').addEventListener('click', changeTempScale);
  document.getElementById('btn-bg').addEventListener('click', getBackground);
  document.querySelector('.search__btn').addEventListener('click', searchCity);
  document.getElementById('lang').addEventListener('change', changeLang);
  setInterval(updateTime, 10000);
  document.querySelector('.search__input').addEventListener('keyup', submitForm);
};
