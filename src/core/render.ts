import { renderHeader } from '../app/components/header/header.component';
import { renderMap } from '../app/components/main/map.component';
import { renderWeather } from '../app/components/main/weather.component';

export const render = async (state: object) => {

  const dom = `
    <div class="wrapper">
    <header class="page-header" id="header">
      ${renderHeader(state)}
    </header>
    <main class="page-main">
      <section class="weather">
        ${renderWeather(state)}
      </section>
      <section class="map">
        ${renderMap(state)}
      </section>
    </main>
  </div>
  `;
  document.body.innerHTML = dom;
};
