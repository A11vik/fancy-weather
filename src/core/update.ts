import { coordToDeg } from '../app/components/main/map.component';
import { data, getIndex } from '../services/dataLang';

export const updateTempScale = ({ isCelsius, temp, feelsLike, tempNextDays, lang }: any): void => {
  const idx = getIndex(lang);
  const currentTemp: Node = document.getElementById('temp-current');
  const feelsLikeDom: Node = document.getElementById('feels-like');
  const nextTemps = document.querySelectorAll('.weather__next-temp');

  if (!isCelsius) {
    currentTemp.textContent = `${fahrenheitFormula(temp)}°`;
    feelsLikeDom.textContent = `${data.feelsLike[idx]}: ${fahrenheitFormula(feelsLike)}°`;
    nextTemps.forEach((el, indx) => {
      el.textContent = `${fahrenheitFormula(tempNextDays[indx])}°`;
    });
  } else {
    currentTemp.textContent = `${temp}°`;
    feelsLikeDom.textContent = `${data.feelsLike[idx]}: ${feelsLike}°`;
    nextTemps.forEach((el, indx) => {
      el.textContent = `${tempNextDays[indx]}°`;
    });
  }
};

export const fahrenheitFormula = (temp: number): number =>
Math.round(((9 / 5) * temp + 32));

export const updateDom = async (state: any) => {
  const { city, country, lat, long, time, feelsLike, humidity, condition, wind,
    season, temp, tempNextDays, isCelsius, date, nameNextDays, lang } = state;

  const idx = getIndex(lang);
  const latObj = coordToDeg(lat);
  const longObj = coordToDeg(long);
  document.querySelector('.weather__town').textContent = `${city}, ${country}`;
  document.querySelector('.weather__time').textContent = `${date}, ${time}`;
  document.getElementById('temp-current').textContent = `${isCelsius ? temp : fahrenheitFormula(temp)}°`;
  document.getElementById('condition').textContent = `${data.condition[condition][idx]}`;
  document.getElementById('feels-like').textContent = `${data.feelsLike[idx]}: ${isCelsius ? feelsLike : fahrenheitFormula(feelsLike)}°`;
  document.getElementById('humidity').textContent = `${data.humidity[idx]}: ${humidity}%`;
  document.getElementById('wind').textContent = `${data.wind[idx]}: ${wind} ${data.windScale[idx]}`;
  document.getElementById('latitude').textContent =
  `${data.lat[idx]}: ${latObj.deg}°${latObj.min}'`;
  document.getElementById('longitude').textContent = `${data.long[idx]}: ${longObj.deg}°${longObj.min}'`;
  const weatherNext = document.querySelectorAll('.weather__next');
  weatherNext.forEach((element, indx) => element.innerHTML = `
    <span>${lang === 'be' ? data.nameDays[nameNextDays[indx]] : nameNextDays[indx]}</span><br>
    <span class="weather__next-temp">${isCelsius ? tempNextDays[indx] : fahrenheitFormula(tempNextDays[indx])}°</span>
  `);
};

export const updateSearchBox = (lang: string): void => {
  const idx = getIndex(lang);
  const { searchCity, search } = data;
  const input = document.querySelector('.search__input') as HTMLFormElement;
  input.placeholder = searchCity[idx];
  const searchBtn = document.querySelector('.search__btn') as HTMLElement;
  searchBtn.innerText = search[idx];
};
